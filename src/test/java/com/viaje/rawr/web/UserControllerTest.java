package com.viaje.rawr.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.viaje.rawr.entity.User;
import com.viaje.rawr.service.UserService;
import com.viaje.rawr.validator.UserValidator;
@ExtendWith(MockitoExtension.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {
	@InjectMocks
	UserController userController;
	 @Test
	    public void testRegistration() 
	    {
	        MockHttpServletRequest request = new MockHttpServletRequest();
	        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	        
	        Model model=Mockito.mock(Model.class);
	        BindingResult bindingResult=Mockito.mock(BindingResult.class);
	        
	        UserValidator userValidator=Mockito.mock(UserValidator.class);
	        
	        UserService userService=Mockito.mock(UserService.class);
	        
	        User userForm=new User();
	        userForm.setNombres("Hugo");
	        userForm.setUsername("hugo001");
	        userController.setUserValidator(userValidator);
	        userController.setUserService(userService);
	        String resultado = 
	        		userController.registration(userForm, bindingResult, model);
	         
	        assertThat(resultado).isEqualTo("solicitud");
	     }
	

}
