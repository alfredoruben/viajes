package com.viaje.rawr.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.viaje.rawr.MongoConfig;
import com.viaje.rawr.entity.User;

@ContextConfiguration(classes={MongoConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MongoOperations mongoOps;
	
	@Test
	public void buscarUsuarioPorUserName() {//findByUsername
	    // given
	    User userTest = new User();
	    userTest.setNombres("Paco"); 
	    userTest.setUsername("paco001");
	    
	    User found = userRepository.findByUsername(userTest.getUsername());
	    if(found==null) {
	    	mongoOps.insert(userTest);
	    	found = userRepository.findByUsername(userTest.getUsername());
	    }	    
	    // then
	    assertThat(found.getUsername())
	      .isEqualTo(userTest.getUsername());
	}
	
	@Test
	public void error_username_duplicado() {//findByUsername
	    // given
	    User userTest = new User();
	    userTest.setNombres("Paco"); 
	    userTest.setUsername("paco001");
	    boolean inserto=false;
	    try {
	    	 mongoOps.insert(userTest);
	    	 inserto=true;
		} catch (Exception e) {
			// TODO: handle exception
		}	    
	    // then
	    assertThat(false)
	      .isEqualTo(inserto);
	}
	
	@Test
	public void guardar_usario() {//findByUsername
	    // given
	    User userTest = new User();
	    userTest.setNombres("Paco2"); 
	    userTest.setUsername("paco002");
	    
	    User found = userRepository.findByUsername(userTest.getUsername());
	    if(found==null) {
	    	userRepository.save(userTest);
	    	found = userRepository.findByUsername(userTest.getUsername());
	    }
    
	    // then
	    assertThat(found.getUsername())
	      .isEqualTo(userTest.getUsername());
	}
	
}
